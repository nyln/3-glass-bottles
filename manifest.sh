#!/bin/bash

trap "kill 0" SIGINT

MANIFEST="$( pwd $( dirname $0 ) )/curseforge-client/manifest.json"
SELF=$( basename $0 )
DATE=$( date +"%Y%m%d%H%M%S" )
VERSION_OVERRIDE="1.18.2"
FLAGS="$@"

while [[ $@ ]]; do
  case $1 in
    "version") [[ $2 ]] && { VERSION_OVERRIDE=$2; shift; } || { echo "Version flag requires a value"; exit 1; } ;;
    "ignore") [[ $2 ]] && { IGNORE_LIST=$2; shift; } || { echo "Ignore flag requires a value"; exit 1; } ;;
    "manifest") [[ $2 ]] && { MANIFEST=$2; shift; } || { echo "Manifest flag requires a value"; exit 1; } ;;
    "update") DO_UPDATE=1 ;;
    "logonly") DO_LOGONLY=1 ;;
    "nolog") DO_NOLOG=1 ;;
    "tags") DO_TAGS=1 ;;
    "forge") DO_FORGE=1 ;;
    "help")
      echo -e "$SELF [FLAGS] <VALUES>\n"
      echo -e " Flag\t\tValues\t\tInformation"
      echo -e " version\t1.18.2\t\tOverride the version to check Curseforge for"
      echo -e " ignore\t\t\"238222\"\tDo not check Curseforge for given project ID's, requires quotes for multiple"
      echo -e " manifest\tmanifest.json\tOverride the default ./curseforge-client/manifest.json location"
      echo -e " update\t\t\t\tPerform a replace of any mods that have a newer file for the given version"
      echo -e " tags\t\t\t\tShow project categories next to the project name"
      echo -e " logonly\t\t\tDo not print anything to terminal, log will be created"
      echo -e " nolog\t\t\t\tDo not create a log file, can be combined with logonly to prevent any logs and no terminal output"
      echo -e " forge\t\t\t\tCheck for latest forge version and update if using in conjunction with update flag"
      echo -e " help\t\t\t\tShow this help screen\n"
      exit
    ;;
  esac
  shift
done

[[ -z $DO_LOGONLY ]] && {
  echo $DATE - $VERSION_OVERRIDE
  echo "Manifest:" $MANIFEST
  echo "FLAGS:" $FLAGS
}
[[ -z $DO_NOLOG ]] && {
  echo $DATE - $VERSION_OVERRIDE >> $SELF-$DATE-$VERSION_OVERRIDE.log
  echo "Manifest:" $MANIFEST >> $SELF-$DATE-$VERSION_OVERRIDE.log
  echo "FLAGS:" $FLAGS >> $SELF-$DATE-$VERSION_OVERRIDE.log
}
[[ ! -z $DO_FORGE ]] && {
  FORGE_LATEST=$( curl -s "https://files.minecraftforge.net/net/minecraftforge/forge/index_1.18.2.html" | grep "Latest:" | sed 's/^.* //g;s/".*$//g' )
  FORGE_CURRENT=$( grep --color=never forge "$MANIFEST" | sed 's/^.*forge-//g;s/".*$//g' )
  [[ ! -z $DO_UPDATE ]] && {
    FLT=$( echo $FORGE_LATEST | sed 's/\.//g' )
    FCT=$( echo $FORGE_CURRENT | sed 's/\.//g' )
    (( FLT > FCT )) && {
      sed -i "s/forge-$FORGE_CURRENT/forge-$FORGE_LATEST/g" "$MANIFEST"
      FORGE_UPDATED="UP"
    }
  }
  [[ -z $DO_LOGONLY ]] && echo -e "Forge Current: $FORGE_CURRENT - Latest $FORGE_LATEST\t$FORGE_UPDATED"
  [[ -z $DO_NOLOG ]] && echo -e "Forge Current: $FORGE_CURRENT - Latest $FORGE_LATEST\t$FORGE_UPDATED" >> $SELF-$DATE-$VERSION_OVERRIDE.log
}

for id in $( cat "$MANIFEST" | jq -r '.files[] | .projectID' ); do
  {
    sleep 0.$(( $RANDOM % 9 + 1 ))
    [[ ! -z $IGNORE_LIST ]] && {
      for IGNORE_ITEM in ${IGNORE_LIST[@]}; do
        [[ $IGNORE_ITEM == $id ]] && SKIP=1  
      done
    }
    [[ ! -z $SKIP ]] && exit 0
    unset project
    STATUS=""
    fileid=$( cat "$MANIFEST" | jq -r '.files[] | .projectID, .fileID' | grep $id -A1 | tail -1 )
    timeout=0
    while [[ -z $project ]]; do
      (( timeout > 2 )) && {
        ERR="Timed out"
        break
      }
      project=$( curl --connect-timeout 1 https://api.cfwidget.com/${id[0]} -s -L )
      (( timeout++ ))
      sleep 0.$(( $RANDOM % 9 + 1 ))
    done
    [[ ! -z $ERR ]] && {
      echo ${id[0]} $ERR
      exit
    }
    projectName=$( echo $project | jq -r '.title' )
    projectLatestFile=$( echo $project | jq -r '.files[] | select(any(.versions[]; . == "Forge") and any(.versions[]; . == "'$VERSION_OVERRIDE'")) | .id' | sort -n | tail -1 )
    [[ ! -z $DO_TAGS ]] && {
      projectTags=$( echo $project | jq -r '.categories[]' )
    }
    [[ $projectLatestFile > $fileid ]] && {
      [[ ! -z $DO_UPDATE ]] && { STATUS="UP"; sed -i "s/$fileid/$projectLatestFile/g" "$MANIFEST"; } || { STATUS="AV"; }
    } || {
      [[ -z $projectLatestFile ]] && { STATUS="NA"; } || { STATUS="OK"; }
    }
    [[ $STATUS == "Unknown" ]] && newfileid="-------" || newfileid=$projectLatestFile
    [[ -z $DO_LOGONLY ]] && {
      echo -e "$id\t$fileid\t$newfileid\t$STATUS\t$projectName\t"$projectTags
    }
    [[ -z $DO_NOLOG ]] && {
    echo -e "$id\t$fileid\t$newfileid\t$STATUS\t$projectName\t"$projectTags >> $SELF-$DATE-$VERSION_OVERRIDE.log
    }
  } &
done
[[ -z $DO_LOGONLY ]] && { echo "Running..."; }
wait
[[ -z $DO_LOGONLY ]] && { echo "Finished"; }
