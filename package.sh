#!/bin/bash

trap "kill 0" SIGINT

SELF=$( basename $0 )
SELF_DIRECTORY="$( pwd $( dirname $0 ) )"
CURSEFORGE_DIRECTORY="$SELF_DIRECTORY/curseforge-client"
MANIFEST_FILE="$CURSEFORGE_DIRECTORY/manifest.json"
RELEASE_DIRECTORY="$SELF_DIRECTORY/release/"
RELEASE_URL="https://gitlab.com/nyln/3-glass-bottles/-/blob/master/release/"
RELEASE_NAME="3-glass-bottles"
RELEASE_TITLE="3 Glass Bottles"
README_FILE="$SELF_DIRECTORY/README.md"
DATE=$( date +"%Y%m%d%H%M%S" )
FLAGS="$@"

## Get sha1sum combination of files in $CURSEFORGE_DIRECTORY
versionCalculate() {
  {
    find "$CURSEFORGE_DIRECTORY" -type f ! -name "version.txt" ! -name "manifest.json" ! -name "*.swp" -exec sha1sum {} \; | awk '{print $1}'
    find "$CURSEFORGE_DIRECTORY" -name "version.txt" -exec cat {} \; | grep -v source |  sha1sum | awk '{print $1}'
    find "$CURSEFORGE_DIRECTORY" -name "manifest.json" -exec cat {} \; | grep -v version | sha1sum | awk '{print $1}'
  } | sha1sum | awk '{print substr($1,1,2)""substr($1,length($1)-4,4)}'
}

## Update relevant version files in $CURSEFORGE_DIRECTORY and $$MANIFEST_FILE
versionUpdate() {
  sed -i "s/source =.*$/source = $1/g" "$CURSEFORGE_DIRECTORY/overrides/config/fancymenu/customization/version.txt"
  sed -i "s/^  \"version\".*$/  \"version\": \"$1\",/g" "$MANIFEST_FILE"
}

## Create a zip file in $RELEASE_DIRECTORY with all relevant files packaged
packageZip() {
  [[ ! -d "$RELEASE_DIRECTORY" ]] && {
    echo "$RELEASE_DIRECTORY doesn't exist, aborting"
    exit 1
  }
  cd "$CURSEFORGE_DIRECTORY"
  ZIP_FILE=$RELEASE_NAME"_"$1".zip"
  zip -rq "$ZIP_FILE" .
  mv "$ZIP_FILE" "$RELEASE_DIRECTORY"
  cd "$SELF_DIRECTORY"
}

## Update the README_FILE with package information
readmeGenerate() {
  printf "Generating README..."
  echo "# $RELEASE_TITLE" > "$README_FILE"
  echo "**Latest version ["$versionOverride"]("$RELEASE_URL""$RELEASE_NAME"_"$versionOverride".zip)**" >> "$README_FILE"
  echo "## Mod List" >> "$README_FILE"

  for id in $( cat "$MANIFEST_FILE" | jq -r '.files[] | .projectID' ); do
    {
      sleep 0.$(( $RANDOM % 9 + 1 ))
      unset project
      fileid=$( cat "$MANIFEST_FILE" | jq -r '.files[] | .projectID, .fileID' | grep $id -A1 | tail -1 )
      timeout=0
      while [[ -z $project ]]; do
        (( timeout > 2 )) && {
          ERR="Timed out"
          break
        }
        project=$( curl --connect-timeout 1 https://api.cfwidget.com/${id[0]} -s -L )
        (( timeout++ ))
        sleep 0.$(( $RANDOM % 9 + 1 ))
      done
      projectName=$( echo $project | jq -r '.title' )
      projectURL=$( echo $project | jq -r '.urls[]' | sort -n | tail -1 )

      echo '- ['$projectName']('$projectURL' "'$id'")' >> "$README_FILE"
    } &
  done
  wait
  printf "Done\n"
}

helpShow() {
  echo -e "$SELF [FLAGS] <VALUES>\n"
  echo -e " Flag\t\tValues\t\t\tInformation"
  echo -e " version\tcalc(ulate)|[value]\tSet the version value to either SHA1SUM calculated or a manually entered value and print to screen"
  echo -e " create\t\t\t\t\tCreate a new packaged zip file in RELEASE_DIRECTORY"
  echo -e " readme\t\t\t\t\tGenerate a new README_FILE with mod ids, names and urls"
  echo -e " help\t\t\t\t\tShow this help screen\n"
  echo -e " example: $SELF version calc create rel readme\n"
  exit
}

[[ ! $1 ]] && helpShow

while [[ $@ ]]; do
  case $1 in
    "version")
      [[ ! $2 ]] && {
        echo "Version flag requires a value"
        exit 1
      }
      case $2 in
        "calculate"|"calc")
          versionOverride=$( versionCalculate )
        ;;
        *)
          versionOverride=$2
        ;;
      esac
      shift
    ;;
    "create")
      versionSet=1
      packageCreate=1
    ;;
    "readme")
      readmeUpdate=1
    ;;
    "help")
      helpShow
    ;;
    *)
      echo "Unknown flag $1"
      exit 1
    ;;
  esac
  shift
done

[[ -z $versionOverride ]] && versionOverride=$( versionCalculate )

echo $RELEASE_TITLE" ["$RELEASE_NAME"] - "$RELEASE_URL
echo "Version set is: $versionOverride"

[[ $versionSet ]] && {
  versionUpdate $versionOverride
}

[[ $packageCreate ]] && {
  packageZip $versionOverride
}

[[ $readmeUpdate ]] && {
  readmeGenerate $versionOverride
}